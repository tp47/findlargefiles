package explorer

import (
	"io/ioutil"
	"log"
)

// Read all files in a directory path and return files and directories seperately
func ReadDir(dirpath string, f, d chan []File) {
	allFiles, err := ioutil.ReadDir(dirpath)
	if err != nil {
		log.Fatal(err)
	}

	var files []File
	var dirs []File
	for _, file := range allFiles {
		if file.IsDir() {
			dirs = append(dirs, File{dirpath + "/" + file.Name(), file.Size()})
		}
		files = append(files, File{dirpath + "/" + file.Name(), file.Size()})
	}
	f <- files
	d <- dirs
}
