package explorer

import "sort"

// Abstracting of a file
type File struct {
	PathName string
	Size     int64
}

// Sort a list of files from maximum to minimum
func sortFiles(files []File) []File {
	sort.Slice(files, func(i, j int) bool {
		return files[i].Size > files[j].Size
	})
	return files
}

// Get the n largest files in a dictory
func MaxNFilesInDir(files []File, numberOfFiles int) []File {
	n := numberOfFiles
	if len(files) < n {
		return sortFiles(files)
	}

	// start with first n files, sort them
	nMaxFiles := sortFiles(files[:n])

	// loop over other files to find files largest than smallestlargefile
	for _, f := range files[n-1:] {
		// sl: Smallest of n LargeFiles
		if sl := nMaxFiles[len(nMaxFiles)-1]; f.Size > sl.Size {
			// append file, sort and truncate to n
			nMaxFiles = append(nMaxFiles, f)
			nMaxFiles = sortFiles(nMaxFiles)[:n]
		}
	}

	return nMaxFiles
}
