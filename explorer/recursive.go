package explorer

// Walk through directories and subdirectories
func Recursive(explore_path string, n int) []File {
	f := make(chan []File, 1000)
	d := make(chan []File, 10)
	go ReadDir(explore_path, f, d)

	files := MaxNFilesInDir(<-f, n)

	for _, dir := range <-d {
		new_path := dir.PathName
		sub_dir_files := Recursive(new_path, n)
		new_files := append(sub_dir_files, files...)
		files = MaxNFilesInDir(new_files, n)
	}
	return files
}
