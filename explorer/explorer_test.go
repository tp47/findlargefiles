package explorer

import (
	"reflect"
	"testing"
)

func TestEmptyDir(t *testing.T) {
	t.Run("For an empty directory return nil", func(t *testing.T) {
		n_largest := 10
		noFile := []File{}
		expected := []File{}
		got := MaxNFilesInDir(noFile, n_largest)
		if !reflect.DeepEqual(expected, got) {
			t.Errorf("Got %v, expected %v", got, expected)
		}
	})
}
func TestSmallDir(t *testing.T) {
	t.Run("For dirs with less files", func(t *testing.T) {
		n_largest := 10
		t.Run("For a directory with a single file return file", func(t *testing.T) {
			expected := []File{File{"first", 10}}
			oneFile := []File{expected[0]}
			got := MaxNFilesInDir(oneFile, n_largest)
			if !reflect.DeepEqual(expected, got) {
				t.Errorf("Got %v, expected %v", got, expected)
			}
		})
		t.Run("For a directory with two files return files in sorted order", func(t *testing.T) {
			expected := []File{File{"first", 10}, File{"asdsa", 5}}
			dir := []File{expected[1], expected[0]}
			got := MaxNFilesInDir(dir, n_largest)
			if !reflect.DeepEqual(expected, got) {
				t.Errorf("Got %v, expected %v", got, expected)
			}
		})
		t.Run("For a directory with three files and return files in sorted order", func(t *testing.T) {
			expected := []File{File{"thrid", 500}, File{"sdfsdf#@#@#@#", 10}, File{"cris", 5}}
			dir := []File{expected[1], expected[0], expected[2]}
			got := MaxNFilesInDir(dir, n_largest)
			if !reflect.DeepEqual(expected, got) {
				t.Errorf("Got %v, expected %v", got, expected)
			}
		})
	})
}

func createMockFiles(n int, base_file_name string, base_file_size int64) []File {
	myFiles := make([]File, n, n)
	for i, one_file := range myFiles {
		one_file.PathName = base_file_name + string(i)
		one_file.Size = base_file_size + int64(i)

	}
	return myFiles
}

func reverseDirFiles(files []File) []File {
	for i := 0; i < len(files)/2; i++ {
		j := len(files) - i - 1
		files[i], files[j] = files[j], files[i]
	}
	return files
}

func TestLargeDir(t *testing.T) {
	n_largest := 10
	t.Run("For a directory with 50 files return 10 largest files", func(t *testing.T) {
		dir := createMockFiles(50, "file", int64(1000))

		idx := len(dir) - n_largest // starting index, 40 in this case
		expected := reverseDirFiles(dir)[idx:]

		got := MaxNFilesInDir(dir, n_largest)

		if !reflect.DeepEqual(expected, got) {
			t.Errorf("Got %v, expected %v", got, expected)
		}
	})

}
