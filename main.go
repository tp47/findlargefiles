package main

import (
	"file-browser-tdd/explorer"
	"fmt"
	"os"
	"strconv"
)

func runExplorer(explore_path string, n_files int) {
	if n_files <= 0 {
		fmt.Println("Invalid number of files.")
		return
	}

	for i, file := range explorer.Recursive(explore_path, n_files) {
		fmt.Println(i+1, file.PathName, file.Size)
	}
}

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Invalid number of command line arguments.")
		fmt.Println("Format: go run main.go [path] [n largest files]")
		return
	}

	size, _ := strconv.Atoi(os.Args[2])
	runExplorer(os.Args[1], size)
}
